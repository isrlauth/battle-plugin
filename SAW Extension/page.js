chrome.runtime.sendMessage('isEnabled', function(enabled) {
	if (enabled) {
		jQuery(function() {
			// Create a custom alert function.
			$.extend({ alert: function (message) {
				$("<div></div>").dialog({
					buttons: [{
						text: 'Ok',
						click: function () { $(this).dialog("close"); },
						'class': 'small-button'
					}],
					close: function (event, ui) { $(this).remove(); },
					resizable: false,
					modal: true,
					create: function() {
						$(".ui-dialog-titlebar").hide();
					}
				}).text(message);
			}});

			// Alert the page what e-mail address is being monitored.
			var pluginPresentInputs = document.body.querySelectorAll(
				'input[type="hidden"][name="saw_hatchet_plugin"]');
			chrome.runtime.sendMessage('getEmail', function(extensionEmail) {
				for (var i = 0; i < pluginPresentInputs.length; i++) {
					pluginPresentInputs[i].value = extensionEmail;
				}
			});
			
			var userEmailInputs = document.body.querySelectorAll(
				'input[type="text"][name="user_email"]');
			chrome.runtime.sendMessage('getEmail', function(extensionEmail) {
				for (var i = 0; i < userEmailInputs.length; i++) {
					userEmailInputs[i].value = extensionEmail;
				}
			});

			// Only sumbit a token if we haven't already done so.
			if (!(/[?&](SAW|Hatchet)Token=/i.test(window.location.href))) {
				// Next check to see if there is a link we should be using.
				var pluginLinks = document.body.querySelectorAll(
					'input[type="hidden"][name="saw_hatchet_plugin_link"]');

				// If the e-mail matches what is in the plugin log in for the user.
				chrome.runtime.sendMessage('getEmail', function(extensionEmail) {
					if (!extensionEmail || extensionEmail === '')
						return;
				
					for (var i = 0; i < pluginLinks.length; i++) {
						var link = pluginLinks[i].value.trim();
						if (link == null || link == '')
							continue;
						if (pluginLinks[i].nextElementSibling.name != 'saw_hatchet_plugin_email')
							continue;
						var linkEmail = pluginLinks[i].nextElementSibling.value.trim().toLowerCase();
						if (linkEmail == null || linkEmail == '')
							continue;

						if (extensionEmail == linkEmail) {
							window.setTimeout(function() { 
								window.location = link;
							}, 0);
						} else {
							/*$.alert('You have the SAW/Hatchet auth plugin enabled, but you have not configured ' +
								'it for use with ' + linkEmail);*/
						}
					}
				});
			}
		});
	}
});