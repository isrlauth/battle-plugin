$(function() {
  // Get the buttons
  var btnSaveSettings = document.getElementById('btnSaveSettings');
  var email = document.getElementById('email');
  var password = document.getElementById('password');

  // Create an alert function.
  $.extend({ alert: function (message, error) {
    var dialog = $("<div></div>").dialog({
      buttons: [{
        text: 'Ok',
        click: function () { $(this).dialog("close"); },
        'class': 'small-button'
      }],
      close: function (event, ui) { $(this).remove(); },
      resizable: false,
      modal: true,
      create: function() {
        $(".ui-dialog-titlebar").hide();
        $(this).parent().css("border-radius", "15px");
        $(this).parent().css("border-width", "5px");
        if (error) {
          $(this).parent().addClass("ui-state-error");
        } else {
          $(this).parent().css("border-color", "green");
        }
      }
    }).text(message);
  }});

  // Set default values.
  email.value = chrome.extension.getBackgroundPage().getEmail();
  password.value = chrome.extension.getBackgroundPage().getPassword();

  // Add a listener to save settings.
  btnSaveSettings.addEventListener('click', function() {
    emailValue = email.value.trim();
    passwordValue = password.value;

    // Ensure the email is correct.
    if (emailValue == '' || !(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(emailValue))) {
      $.alert('Not a valid email.', true);
      return false;
    }

    // Ensure the password looks legitamate
    if (passwordValue.length < 1) {
      $.alert('Incorrect password.', true);
      return false;
    }

    // Save the new value.
    chrome.extension.getBackgroundPage().setEmail(emailValue);
    chrome.extension.getBackgroundPage().setPassword(passwordValue);
    $.alert('Settings saved.', false);
    
    return false;
  });

  password.addEventListener('keypress', function(e) {
    if(e.keyCode == 13) {  
      window.setTimeout(function() { btnSaveSettings.click(); }, 0);
    }
  });
});

// Block the page until we have enabled the plugin.
$(function() {
  var enabled = chrome.extension.getBackgroundPage().isEnabled();
  if (!enabled) {
    $('#mask').show();
    $('#mask button').click(function() {
      $('#mask').hide();
      chrome.extension.getBackgroundPage().setEnabled(true);
    });
  }
});