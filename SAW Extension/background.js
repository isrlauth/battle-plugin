var storage = localStorage;

chrome.runtime.onStartup.addListener(function() {
	// Ensure storage has been cleared.
	storage.enabled = false;
	storage.email = '';
	storage.password = '';
	
});

//Gets a listener for the email.
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  if (request == 'getEmail') {
    sendResponse(getEmail());
  } else if (request == 'isEnabled') {
  	sendResponse(isEnabled());
  }
});

/**
 * Gets whether the plugin is enabled.
 */
var isEnabled = function() {
	return storage.enabled === 'true';
}

/**
 * Gets the stored email.
 * @return {string} Stored email.
 */
var getEmail = function() {
	return storage.email;
}

/**
 * Gets the stored password.
 * @return {string} Stored password.
 */
var getPassword = function() {
	return storage.password;
}

/**
 * Sets whether the plugin is enabled.
 */
var setEnabled = function(enabled) {
	storage.enabled = enabled;
}

/**
 * Sets the stored email.
 * @param email {string} Stored email.
 */
var setEmail = function(email) {
	storage.email = email;
};

/**
 * Sets the stored password.
 * @param password {string} Stored password.
 */
var setPassword = function(password) {
	var fakePassword = '';
	for (var i = 0; i < password.length; i++)
		fakePassword += '*';
	storage.password = fakePassword;
};